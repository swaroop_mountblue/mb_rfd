# LinkedIn

* LinkedIn is a social network that focuses on professional networking and career development.

* LinkedIn is mainly used for professional networking, job hunting, connecting with friends and colleagues, and career management. Most companies also use LinkedIn to recruit and search for potential employees.

## User Persona
  1. ### Bob

     **DEMOGRAPHICS**
        
     * Age : 22
     * Location:  Andhra Pradesh, India.
     * Education : B.Tech in computer science.
    
     **GOAL**

     * Become a Master in software developer.
     
     **Bobs Frustation**

     * Bobs was very confused in starting his career.
     * He is very good at basic fundamentals of computer science.
     * He was confused in choosing technonlogy.
 
  2. ### Allie
     **DEMOGRAPHICS**
      * Age : 35
      * Location:  Bangalore, India.
      * Education : Post-Graduate Degree.
  
     **GOAL**

     * To explore his business in large scale.

     **Bobs Frustation**

     * Allie wants to hire few developers for his business.
     * He doesn't invest much on hiring senior developers.
     * So, he decided to hire freshers and deploy them based on the performance in the bootcamp.
     * But, he is facing difficulty in finding the people who good at basic fundamentals.


## User Stories
* As a non logged in user, ISBAT view a post.
* As a non logged in user, ISBAT view users profile.
* As a logged in user, ISBAT create a post.
* As a logged in user, ISBAT update a post.
* As a logged in user, ISBAT delete a post.
* As a logged in user, ISBAT like a post.
* As a logged in user, ISBAT comment a post.
* As a logged in user, ISBAT view other user profiles.
* As a logged in user, ISBAT message to other users.
* As a logged in user, ISBAT Update my profile.
* As a logged in user, ISBAT delete my account.
* As a logged in user, ISBAT apply for a job.


## Database Schema
1. Users - user_id[primary key],name, email, phonenumber, password, role, organisation, age, experience.
2. post - post[image,text,video],likes, comments, shares, creadtedAt, updatedAt.
3. followers - user_id[foreign key] , follower_user_id
4. following - user_id[foreign key] , following_user_id
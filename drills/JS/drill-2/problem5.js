const {problem4} = require("./problem4.js")

function problem(inventory,year)
{
    return problem4(inventory).split("\t").filter((value)=> value < year)
}

exports.problem5 = problem
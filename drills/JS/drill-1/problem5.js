const {problem4} = require('./problem4.js')

function problem(inventory,year)
{
    let years=[]
    let item;
    for(item of problem4(inventory))
    {
        if(item<year)
        {
            years.push(item)
        }
    }

    return years
}

exports.problem5 = problem
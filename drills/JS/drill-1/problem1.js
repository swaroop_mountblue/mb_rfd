function problem(inventory,id)
{
    for(item of inventory)
    {
        if(item.id==id)
        {
            console.log("Car "+item.id+" is a "+item.car_year+" "+item.car_make+" "+item.car_model+".")
        }
    }
}

exports.problem1 = problem
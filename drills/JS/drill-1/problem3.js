function problem(inventory)
{
    let item;
    let car_models=[]
    for(item of inventory)
    {
        car_models.push(item.car_model.toLowerCase())
    }

    return car_models.sort()
}

exports.problem3 = problem
function problem(inventory)
{
    let item = inventory.pop()
    return "Last car is a "+item.car_make+" "+item.car_model+".";
}

exports.problem2 = problem